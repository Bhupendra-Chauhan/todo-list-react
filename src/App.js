import React from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom'
// import {v4 as uuid} from 'uuid';
import Header from './components/Header';
import AddTodo from './components/AddTodo';
import About from './components/files/About'
import Todo from './components/Todo';
import axios from 'axios';


import './App.css';

class App extends React.Component{
  state = {
    todos :[]
}

componentDidMount(){
  axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
  .then(res=> this.setState({todos: res.data}));

}

markComplete =(id) =>{
  // console.log(e.target)
  this.setState({todos: this.state.todos.map(todo=>{
    if(todo.id===id){
      todo.completed=!todo.completed

    }
    return todo
  })})
}


Delete =(id)=>{
  axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
  .then(res=>this.setState({ todos: this.state.todos.filter(todo=>todo.id!==id) }))
  

}

addTodo=(title)=>{
  //console.log(title)
  axios.post('https://jsonplaceholder.typicode.com/todos',{
    title,
    completed : false
  })
  .then(res=>this.setState({todos: [...this.state.todos, res.data]},()=>console.log(res)))
  
}


  render(){
    return (
      <Router>
        <div className="App">
            <div className="container">
            <Header />
            <Route exact path="/" render={props=>(
              <React.Fragment>
                <AddTodo addTodo={this.addTodo}/>
                <Todo todos={this.state.todos} markComplete={this.markComplete} Delete={this.Delete}/>
              </React.Fragment>
            )} />

            <Route path="/about" component={About} />
            
            </div>
        </div>
      </Router>
    );
  }
}

export default App;

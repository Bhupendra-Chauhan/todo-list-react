import React, { Component } from 'react';
import {Link } from 'react-router-dom'

export class Header extends Component {
    render() {
        return (
            <div>
                <header style={headerStyle}>
                    <Link style={linkStyle} to="/">Home</Link>
                    <h1 style={{color:'Yellow'}}>TodoList App</h1>
                    <Link style={linkStyle} to="/about">About</Link>
                </header>


            </div>
        )
    }
}

const linkStyle={
    textDecoration:'none',
    color:'white',
    fontSize:'1.5rem'
}
const headerStyle={
    display:'flex',
    justifyContent: 'space-between',
    background:'#333',
    color:'#fff',
    padding:'10px',
    textAlign:'center',
    cursor:'pointer'
}

export default Header

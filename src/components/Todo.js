import React, {Component} from 'react'
import TodoItem from './TodoItem'
import PropTypes from 'prop-types';


class Todo extends Component{
    render(){
        // console.log(this.props.todos)
        return this.props.todos.map((arr)=>(
            // 
            <TodoItem key={arr.id} todo={arr} markComplete={this.props.markComplete} Delete={this.props.Delete}/>
        ));
    }
}


Todo.propTypes={
    todos: PropTypes.array.isRequired,
    markComplete: PropTypes.func.isRequired,
    Delete: PropTypes.func.isRequired

}
export default Todo;

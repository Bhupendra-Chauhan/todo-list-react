import React, { Component } from 'react'
import PropTypes from 'prop-types';

export class TodoItem extends Component {
    getStyle =() =>{
        return {
            padding:'10px',
            border:'2px solid grey',

            textDecoration : this.props.todo.completed ? 'line-through' : 'none',
            backgroundColor: this.props.todo.completed ? 'red': 'grey'
            
        }
        
        // if(this.props.todo.completed){
        //     return {
        //         textDecoration: "line-through",
        //         backgroundColor: "red"
        //     }
        // }
        //     else{
        //         return {
        //             textDecoration:"none"
        //         }
        //     }
        
    }

    render() {
        const {id, title, completed}=this.props.todo

        return (
            <div style={this.getStyle()}>
                <p>
                    <input type="checkbox" checked={completed} onChange={this.props.markComplete.bind(this,id)} />
                    
                    {title}
                    <button type="reset" style={{float:'right'}} onClick={this.props.Delete.bind(this,id)}>Delete</button>
                </p>
            </div>
        )
    }
}

TodoItem.propTypes ={
    todo:PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    Delete: PropTypes.func.isRequired
}


export default TodoItem;
